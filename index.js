// ---------------------------------------------------------------------------
//  for-own
//  for...in abstraction to iterate over an objects own keys
// ---------------------------------------------------------------------------

    var iterateUpArray = require('@amphibian/iterate-up-array');
    var objectKeys = require('@amphibian/object-keys');

    /**
     * For own
     * @param {object} object: The Object to iterate over
     * @param {function} callback: Callback function to call upon each iteration
    **/
    function forOwn(object, callback) {
        return iterateUpArray(objectKeys(object), function (index, i, end) {
            callback.call(object, index, object[index], end);
        });
    }

    module.exports = forOwn;
