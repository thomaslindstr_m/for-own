// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var forOwn = require('./index.js');
    var assert = require('assert');

    describe('for-own', function () {
        var object = {
            key: 3,
            foo: 5,
            test: 2
        };

        it('should iterate through the objects 3 keys', function () {
            var i = 0;

            forOwn(object, function () {
                i++;
            });

            assert.equal(3, i);
        });

        it('should set the correct function arguments', function () {
            var i = 0;

            forOwn(object, function (key, value) {
                i += value;
            });

            assert.equal(10, i);
        });

        it('should iterate through the objects 3 keys, but stop at “foo”', function () {
            var i = 0;

            forOwn(object, function (key, value, end) {
                i++;

                if (key === 'foo') {
                    end();
                }
            });

            assert.equal(2, i);
        });

        it('should be able to return the data passed to the end function', function () {
            var data = forOwn(object, function (key, value, end) {
                end('data');
            });

            assert.equal('data', data);
        });

        it('should be able to return an array of data if more arguments were passed', function () {
            var data = forOwn(object, function (key, value, end) {
                end('data', 'is', 'very', 'cool');
            });

            assert.equal(4, Object.keys(data).length);
        });

        it('should return false if calling end with false', function () {
            var data = forOwn(object, function (key, value, end) {
                end(false);
            });

            assert.strictEqual(data, false);
        });
    });
